#!/bin/bash 

wget -qO- https://dl.yarnpkg.com/debian/pubkey.gpg | gpg --import
mkdir -p /tmp/yarn
wget https://github.com/yarnpkg/yarn/releases/download/v$YARN_VER/yarn-v$YARN_VER.tar.gz.asc -O /tmp/yarn/yarn.tar.gz 
wget https://github.com/yarnpkg/yarn/releases/download/v$YARN_VER/yarn-v$YARN_VER.tar.gz.asc -O /tmp/yarn/yarn.tar.gz.asc 
gpg --verify /tmp/yarn/latest.tar.gz.asc 
mkdir -p $HOME/.yarn 
tar zvxf /tmp/yarn/yarn.tar.gz -C $HOME/.yarn --strip-components=1 
export PATH=$HOME/.yarn/bin:$PATH 
rm /tmp/yarn/yarn.tar.* 
rmdir /tmp/yarn