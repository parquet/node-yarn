# node-yarn

Un projet utilisé pour définir les images docker de référence pour le projet Parquet.

les images correspondantes sont générées automatiquement dans DockerHub sous le compte olivr70/

Les images suivantes
- node-yarn : juste node et yarn

## node-yarn

Composants installés :
- yarn : dans $HOME/.yarn

** versions **
- latest: dernière version de node et dernière version de yatn
