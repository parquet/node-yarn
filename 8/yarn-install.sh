#!/bin/bash 

# installation de Yarn à partir des archives TAR

wget -qO- https://dl.yarnpkg.com/debian/pubkey.gpg | gpg --import
# on téléchange dans /tmp/yarn
mkdir -p /tmp/yarn
wget https://github.com/yarnpkg/yarn/releases/download/v$YARN_VER/yarn-v$YARN_VER.tar.gz.asc -O /tmp/yarn/yarn.tar.gz 
wget https://github.com/yarnpkg/yarn/releases/download/v$YARN_VER/yarn-v$YARN_VER.tar.gz.asc -O /tmp/yarn/yarn.tar.gz.asc 
# on vérifie la signature
gpg --verify /tmp/yarn/latest.tar.gz.asc 
# on expanse
mkdir -p $HOME/.yarn 
tar zvxf /tmp/yarn/yarn.tar.gz -C $HOME/.yarn --strip-components=1 
export PATH=$HOME/.yarn/bin:$PATH 
# puis on fait le ménage
rm /tmp/yarn/yarn.tar.* 
rmdir /tmp/yarn