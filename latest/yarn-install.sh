#!/bin/bash 

wget -qO- https://dl.yarnpkg.com/debian/pubkey.gpg | gpg --import
wget https://yarnpkg.com/latest.tar.gz -P /tmp/yarn 
wget https://yarnpkg.com/latest.tar.gz.asc -P /tmp/yarn 
gpg --verify /tmp/yarn/latest.tar.gz.asc 
mkdir -p $HOME/.yarn 
tar zvxf /tmp/yarn/latest.tar.gz -C $HOME/.yarn --strip-components=1 
export PATH=$HOME/.yarn/bin:$PATH 
rm /tmp/yarn/latest.tar.* 
rmdir /tmp/yarn